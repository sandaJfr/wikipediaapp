package com.example.wikiapp.viewmodel

import androidx.lifecycle.ViewModel
import com.example.wikiapp.managers.WikiRepository
import com.example.wikiapp.models.WikiResult

class ExploreViewModel(private val wikiRepository: WikiRepository?) : ViewModel() {


    fun getAllExplorePages() =  wikiRepository?.getExplorePages()

//    fun addExplorePages(pages: List<WikiPage>) = wikiRepository?.addExplorePages(pages)

    fun search(
            term: String,
            skip: Int,
            take: Int,
            resultHandler: (result: WikiResult) -> Unit,
            errorHandler: () -> Unit
    ) = wikiRepository?.search(term, skip, take, resultHandler, errorHandler)

    fun getRandom(take: Int) {
        wikiRepository?.getRandom(take, {
            wikiRepository.addExplorePages(it.query!!.pages)
        }, {})
    }

}