package com.example.wikiapp.models

class WikiThumbnail {
    var source: String? = null
    var width: Int = 0
    var height: Int = 0
}