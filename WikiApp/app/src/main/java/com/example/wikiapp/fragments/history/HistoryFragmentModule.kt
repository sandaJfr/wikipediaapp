package com.example.wikiapp.fragments.history

import com.example.wikiapp.managers.WikiRepository
import com.example.wikiapp.dagger.scopes.FragmentScope
import com.example.wikiapp.viewmodel.factory.HistoryViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class HistoryFragmentModule {

    @Provides
    @FragmentScope
    fun provideHistoryViewModelFactory(repository: WikiRepository) = HistoryViewModelFactory(repository)
}