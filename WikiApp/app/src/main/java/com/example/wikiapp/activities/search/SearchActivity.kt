package com.example.wikiapp.activities.search

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wikiapp.R
import com.example.wikiapp.adapters.ArticleListItemRecyclerAdapter
import com.example.wikiapp.viewmodel.ExploreViewModel
import com.example.wikiapp.viewmodel.factory.ExploreViewModelFactory
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_article_detail.toolbar
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject

class SearchActivity : DaggerAppCompatActivity() {
    private var adapter: ArticleListItemRecyclerAdapter = ArticleListItemRecyclerAdapter()
    private lateinit var exploreViewModel: ExploreViewModel

    @Inject
    lateinit var factory: ExploreViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        exploreViewModel = ViewModelProviders.of(this, factory).get(ExploreViewModel::class.java)

        search_results_recycler.layoutManager = LinearLayoutManager(this)
        search_results_recycler.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchItem = menu.findItem(R.id.action_search)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val searchView = searchItem?.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        searchView.setIconifiedByDefault(false)

        searchView.requestFocus()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    exploreViewModel.search(it, 0, 20, { wikiResult ->
                        adapter.currentResults.clear()
                        wikiResult.query?.pages?.let { pages -> adapter.currentResults.addAll(pages) }
                        runOnUiThread { adapter.notifyDataSetChanged() }
                    }, {})
                }
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                return false
            }
        })


        return super.onCreateOptionsMenu(menu)
    }

}
