package com.example.wikiapp.activities.main

interface MainConnectivityInterface {

   fun getInternetConnection(): Boolean
}