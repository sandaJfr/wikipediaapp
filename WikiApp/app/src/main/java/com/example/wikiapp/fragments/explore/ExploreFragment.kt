package com.example.wikiapp.fragments.explore

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.wikiapp.R
import com.example.wikiapp.activities.main.MainConnectivityInterface
import com.example.wikiapp.activities.search.SearchActivity
import com.example.wikiapp.activities.utilities.toast
import com.example.wikiapp.adapters.ArticleCardRecyclerAdapter
import com.example.wikiapp.viewmodel.ExploreViewModel
import com.example.wikiapp.viewmodel.factory.ExploreViewModelFactory
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_explore.*
import javax.inject.Inject

class ExploreFragment : DaggerFragment() {
    private var adapter: ArticleCardRecyclerAdapter = ArticleCardRecyclerAdapter()

    private lateinit var exploreViewModel: ExploreViewModel

    @Inject
    lateinit var factory: ExploreViewModelFactory

    @Inject
    lateinit var mainConnectivityInterface: MainConnectivityInterface

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_explore, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        search_card_view?.setOnClickListener {
            val searchIntent = Intent(context, SearchActivity::class.java)
            context?.startActivity(searchIntent)
        }

        explore_article_recycler?.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

        explore_article_recycler?.adapter = adapter
        refresher?.setOnRefreshListener {
            if (mainConnectivityInterface.getInternetConnection()) {
                exploreViewModel.getRandom(15)
            } else {
                toast("No internet connection!")
                refresher.isRefreshing = false
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        exploreViewModel = requireActivity().run {
            ViewModelProviders.of(this, factory).get(ExploreViewModel::class.java)
        }
        exploreViewModel.getAllExplorePages()?.observe(this, Observer { pages ->
            if (pages.isEmpty()) {
                if (mainConnectivityInterface.getInternetConnection()) {
                    exploreViewModel.getRandom(15)
                } else {
                    toast("No internet connection!")
                    refresher.isRefreshing = false
                }
            } else {
                pages.let {
                    adapter.run {
                        currentResults.clear()
                        currentResults.addAll(pages)
                        notifyDataSetChanged()
                    }
                    refresher?.isRefreshing = false
                }
            }
        })
    }
}