package com.example.wikiapp.activities.main

import android.content.Context
import android.net.ConnectivityManager
import com.example.wikiapp.dagger.scopes.ActivityScope
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    @ActivityScope
    fun provideConnectivityManager(context: Context) =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
}