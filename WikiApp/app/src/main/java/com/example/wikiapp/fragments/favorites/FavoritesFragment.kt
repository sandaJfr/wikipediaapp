package com.example.wikiapp.fragments.favorites

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.wikiapp.R
import com.example.wikiapp.adapters.ArticleCardRecyclerAdapter
import com.example.wikiapp.viewmodel.FavoritesViewModel
import com.example.wikiapp.viewmodel.factory.FavoritesViewModelFactory
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_favorites.*
import javax.inject.Inject

class FavoritesFragment : DaggerFragment() {
    private val adapter: ArticleCardRecyclerAdapter = ArticleCardRecyclerAdapter()
    private lateinit var favoritesViewModel: FavoritesViewModel

    @Inject
    lateinit var factory: FavoritesViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_favorites, container, false)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        favorites_article_recycler.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        favorites_article_recycler.adapter = adapter

        favoritesViewModel.getFavorites()?.observe(this, Observer { favorites ->
            favorites.let {
                adapter.currentResults.clear()
                adapter.currentResults.addAll(favorites)
                adapter.notifyDataSetChanged()
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        favoritesViewModel = requireActivity().run {
            ViewModelProviders.of(this, factory).get(FavoritesViewModel::class.java)
        }
    }

}
