package com.example.wikiapp.dagger

import com.example.wikiapp.activities.detail.ArticleDetailActivity
import com.example.wikiapp.activities.detail.ArticleDetailModule
import com.example.wikiapp.activities.main.MainActivity
import com.example.wikiapp.activities.main.MainModule
import com.example.wikiapp.activities.search.SearchActivity
import com.example.wikiapp.activities.search.SearchModule
import com.example.wikiapp.dagger.scopes.ActivityScope
import com.example.wikiapp.fragments.FragmetsProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AppBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmetsProvider::class, MainModule::class])
    abstract fun provideMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [ArticleDetailModule::class])
    abstract fun provideArticleDetailActivity(): ArticleDetailActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [SearchModule::class])
    abstract fun provideSearchActivity(): SearchActivity
}