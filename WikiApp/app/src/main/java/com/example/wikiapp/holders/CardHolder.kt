package com.example.wikiapp.holders

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.wikiapp.activities.detail.ArticleDetailActivity
import com.example.wikiapp.models.WikiPage
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.article_card_item.view.*

class CardHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private var currentPage: WikiPage? = null

    init {
        itemView.setOnClickListener {
            val detailPageIntent = Intent(itemView.context, ArticleDetailActivity::class.java)
            val pageJson = Gson().toJson(currentPage)
            detailPageIntent.putExtra("page", pageJson)
            itemView.context.startActivity(detailPageIntent)
        }
    }

    fun updatePage(page: WikiPage) {
        currentPage = page

        itemView.article_title.text = page.title

        page.thumbnail?.let { Picasso.get().load(it.source).into(itemView.article_image) } // load images lazily with picasso

    }
}