package com.example.wikiapp.retrofit

import com.example.wikiapp.models.WikiResult
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val BaseUrl = "https://en.wikipedia.org/w/"

//Retrofit turns HTTP API into a an interface
interface ApiTarget {
    @GET("api.php")
    suspend fun getSearchedWikiPages(
            @Query("action") action: String = "query",
            @Query("formatversion") formatVersion: String = "2",
            @Query("generator") generator: String = "prefixsearch",
            @Query("gpssearch") term: String,
            @Query("gpslimit") take: Int,
            @Query("gpsoffset") skip: Int,
            @Query("prop") prop: String = "pageimages|info",
            @Query("piprop") piprop: String = "thumbnail|url",
            @Query("pithumbsize") pithumbsize: String = "200",
            @Query("pilimit") pilimit: Int,
            @Query("wbptterms") wbptterms: String = "description",
            @Query("format") format: String = "json",
            @Query("inprop") inprop: String = "url"
    ): Response<WikiResult>

    @GET("api.php")
    suspend fun getWikiPages(
            @Query("action") action: String = "query",
            @Query("format") responseFormat: String = "json",
            @Query("formatversion") formatVersion: String = "2",
            @Query("generator") generator: String = "random",
            @Query("grnnamespace") grnnamespace: String = "0",
            @Query("prop") prop: String = "pageimages|info",
            @Query("grnlimit") take: Int,
            @Query("inprop") inprop: String = "url",
            @Query("pithumbsize") pithumbsize: String = "200"
    ): Response<WikiResult>

    // The Retrofit class generates an implementation for the interface
    companion object {
        fun create(): ApiTarget {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BaseUrl)
                    .build()
            return retrofit.create(ApiTarget::class.java)
        }
    }
}