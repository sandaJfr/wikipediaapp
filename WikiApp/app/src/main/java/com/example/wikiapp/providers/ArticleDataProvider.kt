package com.example.wikiapp.providers

import com.example.wikiapp.models.WikiResult
import com.example.wikiapp.retrofit.ApiTarget
import com.github.kittinunf.fuel.core.FuelManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ArticleDataProvider {
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.IO
    private val scope = CoroutineScope(coroutineContext)

    private val retrofit by lazy {
        ApiTarget.create()
    }

    init {
        FuelManager.instance.baseHeaders = mapOf("User-Agent" to "Pluralsight Wikipedia")
    }

    fun search(term: String, skip: Int, take: Int, responseHandler: (WikiResult) -> Unit, errorResponseHandler: () -> Unit) {
        scope.launch {
            val result = retrofit.getSearchedWikiPages(term = term, take = take, skip = skip, pilimit = take)
            if (!result.isSuccessful) {
                errorResponseHandler()
            } else {
                result.body()?.let(responseHandler)
            }
        }
    }

    fun getRandom(take: Int, responseHandler: (WikiResult) -> Unit, errorResponseHandler: (String?) -> Unit) {
        scope.launch {

            val result = retrofit.getWikiPages(take = take)
            if (!result.isSuccessful) {
                errorResponseHandler(result.errorBody()?.string())
            } else {
                result.body()?.let(responseHandler)
            }
        }
    }
}