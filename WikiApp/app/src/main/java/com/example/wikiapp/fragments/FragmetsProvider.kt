package com.example.wikiapp.fragments

import com.example.wikiapp.dagger.scopes.FragmentScope
import com.example.wikiapp.fragments.explore.ExploreFragment
import com.example.wikiapp.fragments.explore.ExploreFragmentModule
import com.example.wikiapp.fragments.favorites.FavoritesFragment
import com.example.wikiapp.fragments.favorites.FavoritesFragmentModule
import com.example.wikiapp.fragments.history.HistoryFragment
import com.example.wikiapp.fragments.history.HistoryFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmetsProvider {

    @ContributesAndroidInjector(modules = [ExploreFragmentModule::class])
    @FragmentScope
    abstract fun provideExploreFragment(): ExploreFragment

    @ContributesAndroidInjector(modules = [FavoritesFragmentModule::class])
    @FragmentScope
    abstract fun provideFavoritesFragment(): FavoritesFragment

    @ContributesAndroidInjector(modules = [HistoryFragmentModule::class])
    @FragmentScope
    abstract fun provideHistoryFragment(): HistoryFragment
}