package com.example.wikiapp.entities

import androidx.room.Entity
import com.example.wikiapp.models.WikiPage

@Entity(tableName = "history")
class History : WikiPage()