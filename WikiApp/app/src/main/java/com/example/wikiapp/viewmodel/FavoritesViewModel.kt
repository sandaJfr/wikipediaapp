package com.example.wikiapp.viewmodel

import androidx.lifecycle.ViewModel
import com.example.wikiapp.managers.WikiRepository
import com.example.wikiapp.models.WikiPage

class FavoritesViewModel(private val wikiRepository: WikiRepository?) : ViewModel() {

    fun getFavorites() = wikiRepository?.getFavorites()

    fun addFavorite(page: WikiPage) = wikiRepository?.addFavorite(page)

    fun removeFavorite(pageId: Int) = wikiRepository?.removeFavorite(pageId)

    fun getIsFavorite(pageId: Int) = wikiRepository?.getIsFavorite(pageId) ?: false

    fun addHistory(page: WikiPage) = wikiRepository?.addHistory(page)

}