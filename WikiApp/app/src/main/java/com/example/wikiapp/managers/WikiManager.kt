package com.example.wikiapp.managers
/*
import androidx.lifecycle.LiveData
import com.example.wikiapp.daos.WikiPageDao
import com.example.wikiapp.entities.Favorites
import com.example.wikiapp.entities.History
import com.example.wikiapp.models.WikiPage
import com.example.wikiapp.models.WikiResult
import com.example.wikiapp.providers.ArticleDataProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class WikiManager(private val remoteProvider: ArticleDataProvider,
                  private val localProvider: WikiPageDao) {
    private var favoritesCache: ArrayList<WikiPage>? = null
    private var historyCache: ArrayList<WikiPage>? = null

    init {
        favoritesCache = getFavorites()
        historyCache = getHistory()
    }

    fun search(
        term: String,
        skip: Int,
        take: Int,
        resultHandler: (result: WikiResult) -> Unit,
        errorHandler: () -> Unit
    ) {
        return remoteProvider.search(term, skip, take, resultHandler, errorHandler)
    }

    fun getRandom(take: Int, resultHandler: (result: WikiResult) -> Unit, errorHandler: () -> Unit) {
        return remoteProvider.getRandom(take, resultHandler, errorHandler)
    }

    @Suppress("UNCHECKED_CAST")
    fun getFavorites(): ArrayList<WikiPage>? {
        runBlocking(Dispatchers.IO) {
            if (favoritesCache == null)
                favoritesCache = localProvider.getAllFavorites().value as ArrayList<WikiPage>
        }

        return favoritesCache
    }

    fun addFavorite(page: WikiPage) {
        GlobalScope.launch(Dispatchers.IO) {
            if (favoritesCache == null)
                favoritesCache?.add(page)
            else if (!favoritesCache!!.any { it.pageid == page.pageid }) { // does not contain page
                favoritesCache?.add(page)
            }
            val favorite: Favorites = Favorites()
            manulMapPages(favorite, page)
            localProvider.addFavorite(favorite)
        }
    }

    fun removeFavorite(pageId: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            localProvider.deleteFavorite(pageId)
            favoritesCache = favoritesCache?.filter { it.pageid != pageId } as ArrayList<WikiPage>
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun getHistory(): ArrayList<WikiPage>? {
        runBlocking(Dispatchers.IO) {
            if (historyCache == null)
                historyCache = localProvider.getAllHistory().value as ArrayList<WikiPage>
        }
        return historyCache
    }

    fun addHistory(page: WikiPage) {
        GlobalScope.launch(Dispatchers.IO) {
            if (historyCache == null)
                historyCache?.add(page)
            else if (!historyCache!!.any { it.pageid == page.pageid }) { // does not contain page
                historyCache?.add(page)
            }
            val history: History = History()
            manulMapPages(history, page)
            localProvider.addHistory(history)
        }
    }

    fun clearHistory() {
        GlobalScope.launch(Dispatchers.IO) {
            historyCache?.clear()
            val allHistory = localProvider.getAllHistory()
            allHistory.value.forEach { localProvider.deleteHistory(it.pageid!!) }
        }
    }

    fun getIsFavorite(pageId: Int): Boolean {
        var isFavorite: Boolean = false
        runBlocking(Dispatchers.IO) {
            var pages = localProvider.getAllFavorites()
            isFavorite = pages.any { page -> page.pageid == pageId }
        }
        return isFavorite
    }

    fun manulMapPages(p1: WikiPage, p2: WikiPage) {
        p1.pageid = p2.pageid
        p1.title = p2.title
        p1.fullurl = p2.fullurl
        p1.thumbnail = p2.thumbnail
    }

    companion object {

        // Volatile = rights to this property are immediately visible to other threads
        @Volatile
        private var instance: WikiManager? = null

        fun getInstance(remoteProvider: ArticleDataProvider, localProvider: WikiPageDao) =
            instance ?: synchronized(this) {
                // lock companion object
                instance ?: WikiManager(remoteProvider, localProvider).also {
                    instance = it
                } // return new instance of QuoteRepository and also set instance to that new one
            }

    }
}
*/
