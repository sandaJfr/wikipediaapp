package com.example.wikiapp.models

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.PrimaryKey

open class WikiPage {
    @PrimaryKey(autoGenerate = false)
    var pageid: Int? = null
    @ColumnInfo(name = "title")
    var title: String? = null
    @ColumnInfo(name = "fullurl")
    var fullurl: String? = null
    @Embedded(prefix = "thumbnail_")
    var thumbnail: WikiThumbnail? = null
}