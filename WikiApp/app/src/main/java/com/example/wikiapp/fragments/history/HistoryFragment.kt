package com.example.wikiapp.fragments.history

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.wikiapp.R
import com.example.wikiapp.adapters.ArticleListItemRecyclerAdapter
import com.example.wikiapp.viewmodel.HistoryViewModel
import com.example.wikiapp.viewmodel.factory.HistoryViewModelFactory
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_history.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 *
 */
class HistoryFragment : DaggerFragment() {
    private val adapter: ArticleListItemRecyclerAdapter = ArticleListItemRecyclerAdapter()
    private lateinit var historyViewModel: HistoryViewModel

    @Inject
    lateinit var factory: HistoryViewModelFactory

    init {
        setHasOptionsMenu(true)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_history, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        history_article_recycler.layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        history_article_recycler.adapter = adapter
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        historyViewModel = requireActivity().run {
            ViewModelProviders.of(this, factory).get(HistoryViewModel::class.java)
        }
    }

    override fun onResume() {
        super.onResume()

        historyViewModel.getHistory()?.observe(this, Observer { history ->
            history.let {
                adapter.currentResults.clear()
                adapter.currentResults.addAll(history)
                activity?.runOnUiThread {
                    adapter.notifyDataSetChanged()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.history_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_clear_history){
            //show confirmation alert
            val builder = AlertDialog.Builder(this.context)

            builder.setTitle("Confirm")
            builder.setMessage("Are you sure you want to clear your history?")

            builder.setPositiveButton("YES") { dialog, _ ->
                adapter.currentResults.clear()
                historyViewModel.clearHistory()
                activity?.run { adapter.notifyDataSetChanged() }
                dialog.dismiss()
            }

            builder.setNegativeButton("NO") { dialog, _ ->
                //Do nothing
                dialog.dismiss()
            }

            val alert = builder.create()
            alert.show()
        }
        return true
    }

}
