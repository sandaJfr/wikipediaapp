package com.example.wikiapp.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.wikiapp.entities.Explore
import com.example.wikiapp.entities.Favorites
import com.example.wikiapp.entities.History

@Dao
interface WikiPageDao {

    /*------------------ Explore --------------------*/

    @Query("SELECT * from explore")
    fun getAllExplorePages(): LiveData<List<Explore>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllExplorePages(pages: List<Explore>)

    @Query("DELETE FROM explore")
    fun deleteAllExplorePages()

    @Transaction
    fun replaceExplorePages(pages: List<Explore>){
        deleteAllExplorePages()
        insertAllExplorePages(pages)
    }

    /*------------------ Favorites ------------------*/
    @Query("SELECT * FROM favorites")
    fun getAllFavorites(): LiveData<List<Favorites>>

    @Query("SELECT COUNT(*) FROM favorites WHERE pageid = :pageId")
    fun isFavourite(pageId: Int): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addFavorite(favorites: Favorites)

    @Query("DELETE FROM favorites WHERE pageid == :pageId")
    fun deleteFavorite(pageId: Int)

    /*------------------ History ---------------------*/

    @Query("SELECT * FROM history")
    fun getAllHistory(): LiveData<List<History>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addHistory(vararg history: History)

    @Query("DELETE FROM history WHERE pageid == :pageId")
    fun deleteHistory(pageId: Int)

    @Query("DELETE FROM history")
    fun clearHistory()
}