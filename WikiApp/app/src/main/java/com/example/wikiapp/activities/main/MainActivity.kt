package com.example.wikiapp.activities.main

import android.net.ConnectivityManager
import android.net.Network
import android.os.Bundle
import com.example.wikiapp.R
import com.example.wikiapp.fragments.explore.ExploreFragment
import com.example.wikiapp.fragments.favorites.FavoritesFragment
import com.example.wikiapp.fragments.history.HistoryFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), MainConnectivityInterface {
    @Inject
    lateinit var connectivityManager: ConnectivityManager
    private var hasInternet = false

    private var connectivityListener = object : ConnectivityManager.NetworkCallback() {
        override fun onLost(network: Network) {
            super.onLost(network)
            hasInternet = false
        }

        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            hasInternet = true
        }
    }

    override fun getInternetConnection(): Boolean = hasInternet

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)

        when (item.itemId) {
            R.id.navigation_explore -> transaction.replace(R.id.fragment_container, ExploreFragment())
            R.id.navigation_favorites -> transaction.replace(R.id.fragment_container, FavoritesFragment())
            R.id.navigation_history -> transaction.replace(R.id.fragment_container, HistoryFragment())

        }
        transaction.commit()

        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        nav_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragment_container, ExploreFragment())
        transaction.commit()
        connectivityManager.registerDefaultNetworkCallback(connectivityListener)

    }

    override fun onDestroy() {
        super.onDestroy()
        connectivityManager.unregisterNetworkCallback(connectivityListener)
    }


}
