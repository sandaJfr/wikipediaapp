package com.example.wikiapp.fragments.favorites

import com.example.wikiapp.managers.WikiRepository
import com.example.wikiapp.dagger.scopes.FragmentScope
import com.example.wikiapp.viewmodel.factory.FavoritesViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class FavoritesFragmentModule {

    @Provides
    @FragmentScope
    fun provideFavoritesViewModelFactory(repository: WikiRepository) =
        FavoritesViewModelFactory(repository)
}