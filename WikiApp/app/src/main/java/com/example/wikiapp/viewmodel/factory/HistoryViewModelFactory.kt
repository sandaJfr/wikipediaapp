package com.example.wikiapp.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.wikiapp.managers.WikiRepository
import com.example.wikiapp.viewmodel.HistoryViewModel

@Suppress("UNCHECKED_CAST")
class HistoryViewModelFactory(private val wikiRepository: WikiRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = HistoryViewModel(
        wikiRepository
    ) as T
}