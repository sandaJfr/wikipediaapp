package com.example.wikiapp.activities.detail

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.example.wikiapp.R
import com.example.wikiapp.models.WikiPage
import com.example.wikiapp.viewmodel.FavoritesViewModel
import com.example.wikiapp.viewmodel.factory.FavoritesViewModelFactory
import com.google.gson.Gson
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_article_detail.*
import javax.inject.Inject

class ArticleDetailActivity : DaggerAppCompatActivity() {

    private var currentPage: WikiPage? = null
    private lateinit var favoritesViewModel: FavoritesViewModel

    @Inject
    lateinit var factory: FavoritesViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_detail)

        val wikiPageJson = intent.getStringExtra("page")
        currentPage = Gson().fromJson(wikiPageJson, WikiPage::class.java)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        favoritesViewModel = ViewModelProviders.of(this, factory).get(FavoritesViewModel::class.java)
        supportActionBar?.title = currentPage?.title

        article_detail_webview.webViewClient = WebViewClient()

        article_detail_webview.loadUrl(currentPage?.fullurl)

        currentPage?.let { favoritesViewModel.addHistory(it) }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.article_menu, menu)

        val item = menu.findItem(R.id.action_favorite)

        currentPage?.pageid?.let {
            if (favoritesViewModel.getIsFavorite(it))
                item?.setIcon(R.drawable.ic_favorite_24dp)
            else
                item?.setIcon(R.drawable.ic_outline_favorite_white_24dp)
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) { //handle if back button is selected (clicked)
            finish()
        } else if (item.itemId == R.id.action_favorite) {
            try {
                currentPage?.let { page ->
                    page.pageid?.let {
                        if (favoritesViewModel.getIsFavorite(it)) {
                            favoritesViewModel.removeFavorite(it)
                            item.setIcon(R.drawable.ic_outline_favorite_white_24dp)
                            Toast.makeText(this, "Article removed from favorites!", Toast.LENGTH_SHORT).show()
                        } else {
                            favoritesViewModel.addFavorite(page)
                            item.setIcon(R.drawable.ic_favorite_24dp)
                            Toast.makeText(this, "Article added to favorites!", Toast.LENGTH_SHORT).show()
                        }
                    }
                }

            } catch (ex: Exception) {
                Toast.makeText(this, "Unable to update this article!", Toast.LENGTH_SHORT).show()
            }
        }
        return true
    }
}