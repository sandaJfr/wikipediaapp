package com.example.wikiapp.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.wikiapp.daos.WikiPageDao
import com.example.wikiapp.entities.Explore
import com.example.wikiapp.entities.Favorites
import com.example.wikiapp.entities.History


@Database(entities = [Favorites::class, History::class, Explore::class], version = 1, exportSchema = false)
abstract class WikiDataBase : RoomDatabase() {
    abstract fun wikiPageDao(): WikiPageDao

    companion object {
        var instance: WikiDataBase? = null

        fun getWikiDataBase(context: Context): WikiDataBase? {
            if (instance == null) {
                synchronized(WikiDataBase::class) {
                    instance = Room.databaseBuilder(context.applicationContext, WikiDataBase::class.java, "wikiDataBase").build()
                }
            }
            return instance
        }
    }
}