package com.example.wikiapp.activities.search

import com.example.wikiapp.dagger.scopes.ActivityScope
import com.example.wikiapp.managers.WikiRepository
import com.example.wikiapp.viewmodel.factory.ExploreViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class SearchModule {

    @Provides
    @ActivityScope
    fun providesExploreViewModelFactory(repository: WikiRepository) = ExploreViewModelFactory(repository)
}