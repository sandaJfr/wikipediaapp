package com.example.wikiapp.viewmodel

import androidx.lifecycle.ViewModel
import com.example.wikiapp.managers.WikiRepository

class HistoryViewModel(private val wikiRepository: WikiRepository?) : ViewModel() {

    fun getHistory() = wikiRepository?.getHistory()

    fun clearHistory() = wikiRepository?.clearHistory()

}