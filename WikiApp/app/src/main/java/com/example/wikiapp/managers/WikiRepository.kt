package com.example.wikiapp.managers

import androidx.lifecycle.LiveData
import com.example.wikiapp.daos.WikiPageDao
import com.example.wikiapp.entities.Explore
import com.example.wikiapp.entities.Favorites
import com.example.wikiapp.entities.History
import com.example.wikiapp.models.WikiPage
import com.example.wikiapp.models.WikiResult
import com.example.wikiapp.providers.ArticleDataProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class WikiRepository(
        private val remoteProvider: ArticleDataProvider,
        private val localProvider: WikiPageDao) {

    fun search(
            term: String,
            skip: Int,
            take: Int,
            resultHandler: (result: WikiResult) -> Unit,
            errorHandler: () -> Unit
    ) = remoteProvider.search(term, skip, take, resultHandler, errorHandler)

    fun getRandom(take: Int, resultHandler: (result: WikiResult) -> Unit, errorHandler: (String?) -> Unit) =
            remoteProvider.getRandom(take, resultHandler, errorHandler)

    @Suppress("UNCHECKED_CAST")
    fun getFavorites(): LiveData<List<WikiPage>> = localProvider.getAllFavorites() as LiveData<List<WikiPage>>

    fun addFavorite(page: WikiPage) {
        GlobalScope.launch {
            val favorite = Favorites()
            manulMapPages(favorite, page)
            localProvider.addFavorite(favorite)
        }
    }

    fun removeFavorite(pageId: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            localProvider.deleteFavorite(pageId)
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun getHistory(): LiveData<List<WikiPage>> = localProvider.getAllHistory() as LiveData<List<WikiPage>>

    fun addHistory(page: WikiPage) {
        GlobalScope.launch(Dispatchers.IO) {
            val history = History()
            manulMapPages(history, page)
            localProvider.addHistory(history)
        }
    }

    fun clearHistory() {
        GlobalScope.launch(Dispatchers.IO) {
            localProvider.clearHistory()
        }
    }

    fun getIsFavorite(pageId: Int): Boolean {
        var isFavorite = false
        runBlocking(Dispatchers.IO) {
            isFavorite = localProvider.isFavourite(pageId) > 0
        }
        return isFavorite
    }

    fun manulMapPages(p1: WikiPage, p2: WikiPage) {
        p1.pageid = p2.pageid
        p1.title = p2.title
        p1.fullurl = p2.fullurl
        p1.thumbnail = p2.thumbnail
    }

    fun addExplorePages(pages: List<WikiPage>){
        GlobalScope.launch(Dispatchers.IO) {
            val list = ArrayList<Explore>()
            pages.forEach {
                val explore = Explore()
                manulMapPages(explore, it)
                list.add(explore)
            }
            localProvider.replaceExplorePages(list)
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun getExplorePages(): LiveData<List<WikiPage>> = localProvider.getAllExplorePages() as LiveData<List<WikiPage>>
}
