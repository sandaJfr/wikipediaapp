package com.example.wikiapp.dagger

import android.app.Application
import android.content.Context
import com.example.wikiapp.daos.WikiPageDao
import com.example.wikiapp.databases.WikiDataBase
import com.example.wikiapp.managers.WikiRepository
import com.example.wikiapp.providers.ArticleDataProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(app: Application): Context = app

    @Provides
    @Singleton
    internal fun provideDao(context: Context): WikiPageDao = WikiDataBase.getWikiDataBase(context)!!.wikiPageDao()

    @Provides
    @Singleton
    internal fun provideArticleDataProvider(): ArticleDataProvider = ArticleDataProvider()

    @Provides
    @Singleton
    internal fun provideRepository(localProvider: WikiPageDao, remoteProvider: ArticleDataProvider): WikiRepository = WikiRepository(remoteProvider, localProvider)
}