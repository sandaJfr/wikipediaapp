package com.example.wikiapp.fragments.explore

import com.example.wikiapp.activities.main.MainActivity
import com.example.wikiapp.activities.main.MainConnectivityInterface
import com.example.wikiapp.dagger.scopes.FragmentScope
import com.example.wikiapp.managers.WikiRepository
import com.example.wikiapp.viewmodel.factory.ExploreViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ExploreFragmentModule {

    @Provides
    @FragmentScope
    fun provideExploreViewModelFactory(repository: WikiRepository) =
            ExploreViewModelFactory(repository)

    @Provides
    @FragmentScope
    fun provideMainConnectivityInterface(mainActivity: MainActivity): MainConnectivityInterface = mainActivity
}