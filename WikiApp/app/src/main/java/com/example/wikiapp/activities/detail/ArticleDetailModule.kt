package com.example.wikiapp.activities.detail

import com.example.wikiapp.dagger.scopes.ActivityScope
import com.example.wikiapp.managers.WikiRepository
import com.example.wikiapp.viewmodel.factory.FavoritesViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ArticleDetailModule {
    @Provides
    @ActivityScope
    fun provideFavoritesViewModelFactory(repository: WikiRepository) =
        FavoritesViewModelFactory(repository)
}